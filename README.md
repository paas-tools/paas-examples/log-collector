# PaaS Log Collector

This repository documents how to create a custom log collector that ingests logs from an application, parses them and forwards them to an external logging system.

The [implementation section](#implementation) provides a step-by-step guide how to parse logs in the `combined` log format (used by NGINX and Apache) and send them with [Vector](https://vector.dev/) to a [MONIT producer](https://monit-docs.web.cern.ch/logs/http/) or [OpenSearch instance](https://esdocs.web.cern.ch/cluster_request/).

## Motivation

PaaS project owners already have access to their logs in the shared OpenSearch instance, see ["Logs" in the PaaS user documentation](https://paas.docs.cern.ch/7._Monitoring/2-logs/).

However, this logging pipeline has some limitation due to its shared nature (log retention not configurable, no support for custom log processing, log format not customizable).
Therefore project owners may want a separate log processing pipeline under their control:
the setup described in this document allows processing arbitrary log formats (e.g. parsing NGINX or Apache logs) and sending the logs to any number of external logging systems (e.g. OpenSearch cluster).

## Overview

1) Create a shared volume between containers in the same pod with a [Kubernetes emptyDir](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir)
2) Reconfigure the application to write logs to a file (instead of `stdout`)
3) Add a sidecar container to the application pod and configure it
4) Setup log rotation to cleanup old log files from the shared volume

```
+--------------------------------------------------------------+
|                                                              |
|  +-----------------------+      +-----------------------+    |
|  |                       |      |                       |    |
|  |      Application      |      |     Log Collector     |    |
|  |                       |      |                       |    |
|  |  (NGINX, Apache, ..)  |      | (Vector, Fluentd, ..) |    | Sends logs to external system
|  |                       |      |                       |    |
|  |                   |   |      |   ^                   | ---+----->   (OpenSearch, MONIT, ...)
|  +-------------------+---+      +---+-------------------+    |
|                      |              |                        |
|                      |              |                        |
|      Writes to       |              |   Reads from           |
|      file or pipe    |              |   file or pipe         |
|                      |              |                        |
|                      v              |                        |
|  +----------------------------------+-------------------+    |
|  |              Shared, ephemeral volume                |    |
|  +------------------------------------------------------+    |
|                                                              |
+--------------------------------------------------------------+
```

## Implementation

We recommend [Vector](https://vector.dev/docs/): a logging agent written in Rust with the goals of high performance, efficiency and resiliency.

To get started, let's create a simple nginx web server:

```sh
oc create deployment nginx --image=registry.cern.ch/ghcr.io/nginxinc/nginx-unprivileged:latest --port=8080
oc expose deployment nginx
oc expose service nginx
```

Let's check the status and get the URL of the application:

```sh
$ oc get pods
NAME                                            READY   STATUS    RESTARTS   AGE
nginx-548ff5d5cb-vj7f6                          1/1     Running   0          31s

$ oc get route
NAME                           HOST/PORT                              PATH      SERVICES                       PORT        TERMINATION     WILDCARD
nginx                          nginx-jh-test.apptest.cern.ch                    nginx                          8080-tcp                    None
```

Connect to the application:

```sh
$ curl -I http://nginx-jh-test.apptest.cern.ch
HTTP/1.1 200 OK
server: nginx/1.25.2
date: Fri, 15 Sep 2023 10:07:57 GMT
content-type: text/html
content-length: 615
last-modified: Tue, 15 Aug 2023 17:03:04 GMT
etag: "64dbafc8-267"
accept-ranges: bytes
set-cookie: 8a72bea50b4cbdc3a7cebd0a74495b0e=b5f0b55124fb3918d429448ab61fe478; path=/; HttpOnly
cache-control: private
```

It works!

> **Note:** the setup above does not create an HTTPS route - only HTTP is available

We can check the logs of nginx:

```sh
$ oc logs deployment/nginx
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/09/15 09:57:45 [notice] 1#1: using the "epoll" event method
2023/09/15 09:57:45 [notice] 1#1: nginx/1.25.2
2023/09/15 09:57:45 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/09/15 09:57:45 [notice] 1#1: OS: Linux 6.1.18-200.fc37.x86_64
2023/09/15 09:57:45 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2023/09/15 09:57:45 [notice] 1#1: start worker processes
10.76.15.1 - - [15/Sep/2023:10:07:57 +0000] "HEAD / HTTP/1.1" 200 0 "-" "curl/8.2.1" "188.184.179.189"
```

The last line of the log above show the curl request from before.

We have verified the basic setup, now we can start to implement the steps outlined in the [Overview](#overview).

### 1) Add a shared volume

We need to modify the deployment we created previously.
This can be done from the OpenShift Web Console with the YAML view or directly with the CLI: `oc edit deployment/nginx`.

In the `spec:template:spec` section we need to introduce a new "volume" section and add a "volumeMount" to the existing `containers` section:

```yaml
spec:
  # ...
  template:
    # ...
    spec:
      # ADD THE LINES BELOW
      volumes:
        - name: shared
          emptyDir: {}

      containers:
      - name: nginx-unprivileged

        # ADD THE LINES BELOW
        volumeMounts:
          - name: shared
            mountPath: /logs
```

![](nginx-add-volumes.png)

We should confirm that the pod is running after these changes, then we can proceed to the next step:

```sh
$ oc get pods
NAME                                            READY   STATUS    RESTARTS   AGE
nginx-6df47f8fb-r2rnf                           1/1     Running   0          26s
```

### 2) Reconfigure application to write logs to file

How exactly this is done will be different from one application to another.
Generally speaking, all web servers have the concept of an "access log", i.e. log entries only related to incoming requests, not error logs etc.

In this case, we need to set the [NGINX access_log setting](https://nginx.org/en/docs/http/ngx_http_log_module.html):

```conf
access_log /logs/access.log combined;
```

> **Note**: you can find the complete [NGINX configuration file in this repository](./default.conf).

For this purpose we create a ConfigMap and mount it into the container:

```sh
$ oc create configmap nginx-config --from-file=default.conf
```

To mount this configuration file permanently into the nginx container, we need to modify the Deployment again (with the Web Console or `oc edit deployment/nginx`):

```yaml
    # ...
    spec:
      volumes:
        - name: shared
          emptyDir: {}
        # ADD THE ITEM BELOW
        - name: nginx-config
          configMap:
            name: nginx-config

      containers:
      - name: nginx-unprivileged
        volumeMounts:
          - name: shared
            mountPath: /logs
          # ADD THE LINES BELOW
          - name: nginx-config
            mountPath: /etc/nginx/conf.d/
```

Again, ensure the container is running.
If we perform a request to the webserver now, it should no longer appear in the `stdout` of the container.

![](nginx-add-configmap.png)

### 3) Add logging sidecar

To read the logs from the file and send them to an external system, we will use [Vector](https://vector.dev/docs/setup/quickstart/) because it is fast, lightweight and supports [many log sinks](https://vector.dev/docs/reference/configuration/sinks/).

There are three types of directives in Vector's configuration:

* *Sources* define targets to collect data from - `[source.*]` elements in the TOML config.
* *Transformations* define how to parse unstructured data into structured data and transform it. - `[transforms.*]` elements.
* *Sinks* define destinations to send/store structured data to - `[sinks.*]` elements.

Transformations are optional, i.e. it is possible to directly connect a sink to a source.
These *connections* are made by specifying the `inputs = ["name-of-my-source-or-transform"]` parameter in each sink.

The example in `vector-config.toml` shows three types of sinks:

* the `console` sink for writing logs to stdout (useful for debugging the log format),
* the `elasticsearch` sink for sending logs to an [ElasticSearch or OpenSearch cluster](https://esdocs.web.cern.ch/),
* the `http` sink for sending JSON-formatted logs to an HTTP endpoint, such as [MONIT logs](https://monit-docs.web.cern.ch/logs/http/);

The Vector documentation has [more setup guides available](https://vector.dev/guides/) and also offers a [playground environment for the Vector Remap Language (VRL)](https://playground.vrl.dev/).

Since it may contain credentials, we will use a Secret (instead of a ConfigMap) to store this configuration in the cluster:

> **Note**: you can find the complete [Vector configuration file in this repository](./vector.toml).
> Please make sure to adjust the file appropriately before running the following command.
> In particular, all placeholders in `<>` brackets need to be replaced.

```sh
$ oc create secret generic vector-config --from-file=vector.toml
```

Again, we need to add this secret as a volume to the deployment:

```yaml
    ...
    spec:
      volumes:
      - name: shared
        emptyDir: {}
      - name: nginx-config
        configMap:
          name: nginx-config
      # ADD THE ITEM BELOW
      - name: vector-config
        secret:
          secretName: vector-config
```

Finally, we add a new container (the *sidecar*) to the existing deployment:

```yaml
    # ...
    spec:
      containers:
      - name: nginx-unprivileged

      # ADD THE LINES BELOW
      - name: vector
        image: registry.cern.ch/docker.io/timberio/vector:0.29.1-alpine
        volumeMounts:
        - name: vector-config
          mountPath: /etc/vector
        - mountPath: /logs
          name: shared
        env:
        - name: MONIT_PRODUCER
          value: "<NAME_OF_YOUR_MONIT_PRODUCER>"
```

![](vector-sidecar.png)

Verify that the pod is running again (now with two containers!):

```sh
$ oc get pods
NAME                                            READY   STATUS    RESTARTS   AGE
nginx-74549d4989-jnqt7                          2/2     Running   0          2m1s
```

Perform new HTTP requests and inspect the logs of the "vector" container:

```sh
$ curl -sI http://nginx-jh-test.apptest.cern.ch > /dev/null

$ oc logs deploy/nginx -c vector
2023-09-15T12:12:03.779274Z  INFO vector::app: Log level is enabled. level="vector=info,codec=info,vrl=info,file_source=info,tower_limit=trace,rdkafka=info,buffers=info,lapin=info,kube=info"
2023-09-15T12:12:03.779926Z  INFO vector::app: Loading configs. paths=["/etc/vector/vector.toml"]
2023-09-15T12:12:03.784018Z  INFO vector::topology::running: Running healthchecks.
2023-09-15T12:12:03.784143Z  INFO vector::topology::builder: Healthcheck passed.
2023-09-15T12:12:03.784172Z  INFO vector: Vector has started. debug="false" version="0.29.1" arch="x86_64" revision="74ae15e 2023-04-20 14:50:42.739094536"
2023-09-15T12:12:03.784220Z  INFO vector::app: API is disabled, enable by setting `api.enabled` to `true` and use commands like `vector top`.
2023-09-15T12:12:03.784239Z  INFO source{component_kind="source" component_id=file_logs component_type=file component_name=file_logs}: vector::sources::file: Starting file server. include=["/logs/*.log"] exclude=[]
2023-09-15T12:12:03.784712Z  INFO source{component_kind="source" component_id=file_logs component_type=file component_name=file_logs}:file_server: file_source::checkpointer: Attempting to read legacy checkpoint files.
2023-09-15T12:12:03.785118Z  WARN source{component_kind="source" component_id=file_logs component_type=file component_name=file_logs}:file_server: vector::internal_events::file::source: Currently ignoring file too small to fingerprint. file=/logs/access.log
2023-09-15T12:12:38.635099Z  INFO source{component_kind="source" component_id=file_logs component_type=file component_name=file_logs}:file_server: vector::internal_events::file::source: Found new file to watch. file=/logs/access.log
{"agent":"curl/7.76.1","file":"/logs/access.log","host":"10.76.17.1","message":"HEAD / HTTP/1.1","method":"HEAD","path":"/","protocol":"HTTP/1.1","referrer":"-","size":0,"source_type":"file","status":200,"timestamp":"2023-09-15T12:12:38Z"}
{"agent":"curl/7.76.1","file":"/logs/access.log","host":"10.76.17.1","message":"HEAD / HTTP/1.1","method":"HEAD","path":"/","protocol":"HTTP/1.1","referrer":"-","size":0,"source_type":"file","status":200,"timestamp":"2023-09-15T12:12:39Z"}
```

The log lines above show that vector successfully initialized, started reading logs from `/logs/access.log`, parsed the NGINX "combined" log format correctly and outputs them as JSON.

### 4) Log rotation

Since the application is writing the log files into a local directory, we should cleanup this directory regularly to prevent old log files from accumulating or growing too large.
In the case of NGINX this can be done by sending it a `USR1` signal which instructs NGINX to re-open its log files ([ref](https://www.nginx.com/resources/wiki/start/topics/examples/logrotation/)).

> **Note**: while this mechanism is common, log rotation needs to be handled differently for each application.

Edit the deployment and add another sidecar container:

```yaml
    spec:
      # THIS SETTING IS IMPORTANT FOR SENDING SIGNALS BETWEEN CONTAINERS
      shareProcessNamespace: true

      containers:
      # ...
      - name: logrotate
        # note that we reuse the same image as for the vector container
        # since it has the necessary tools installed - this avoids downloading another image
        image: registry.cern.ch/docker.io/timberio/vector:0.29.1-alpine
        # the following command resets the log file once per hour
        command:
        - sh
        - -xc
        - while sleep 3600; do rm -f /logs/access.log; pkill -USR1 "nginx: master process"; done
        volumeMounts:
        - mountPath: /logs
          name: shared
```

![](log-rotation-sidecar.png)

If we are patient (or change the logrotate interval), we can see the log rotation in action:

```sh
$ oc logs deployment/nginx -c logrotate
+ sleep 3600
+ rm -f /logs/access.log
+ pkill -USR1 nginx
+ sleep 3600
...

$ oc logs deployment/nginx -c nginx
2023/09/15 14:10:50 [notice] 7#7: reopening logs
2023/09/15 14:10:50 [notice] 27#27: signal 10 (SIGUSR1) received from 76, reopening logs

$ oc logs deployment/nginx -c vector
2023-09-15T14:10:50.495807Z  INFO vector::internal_events::file::source: Found new file to watch. file=/logs/access.log
```

> **Note**: NGINX only creates the log file after serving the first request.

## Production recommendations

We have successfully configured a web server (NGINX) with the "combined" log format and instructed it to write those logs to a file.
From there, the log collector sidecar (Vector) reads the logs, parses them and forwards them to an external log storage system.

To keep the length of this guide reasonable, a few details were skipped.
It is nevertheless important to perform the following steps for a production-grade setup:

* Configure appropriate CPU and memory [resource requests and limits](https://paas.docs.cern.ch/6._Quota_and_resources/2-application-resources/) (based on your usage) for NGINX and Vector

```yaml
containers:
- name: nginx
  resources:
    requests:
      memory: 32Mi
      cpu: 20m
    limits:
      memory: 120Mi
      cpu: 500m
# ...
- name: vector
  resources:
    requests:
      memory: 32Mi
      cpu: 20m
    limits:
      memory: 120Mi
      cpu: 500m
```


* Configure [Liveness and Readiness probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for NGINX

```conf
# nginx.conf
location /_/healthz {
  access_log off;
  return 200
}
```

```yaml
containers:
- name: nginx
  readinessProbe:
    httpGet:
      path: /_/healthz
      port: http
  livenessProbe:
    httpGet:
      path: /_/healthz
      port: http
```

* Enable the Vector API (but do not expose it with a Service) and configure a [Liveness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/) for the container

```toml
# vector.toml
[api]
enabled = true
address = "127.0.0.1:8686"
```

```yaml
containers:
- name: vector
  livenessProbe:
    httpGet:
      path: /health
      port: 8686
```

* Set up monitoring for log ingestion to ensure you're not loosing data or accidentally sending too much - see [PaaS docs: Custom alerts](https://paas.docs.cern.ch/7._Monitoring/1-metrics/#defining-custom-alerts)
* Review the Vector buffer and batch configuration to fine-tune performance and resiliency - see also: [Vector: Going to Production](https://vector.dev/docs/setup/going-to-prod/)
